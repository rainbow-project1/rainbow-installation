# Special Case - If there are NVIDIA Xavier devices on the cluster

## CJDNS enablement for Xavier

### Update and download the correct kernel
```
sudo apt-get update
sudo apt-get install -y libncurses5-dev
mkdir -p jetson-bsp-sources
cd jetson-bsp-sources
wget https://developer.nvidia.com/embedded/l4t/r32_release_v5.1/r32_release_v5.1/sources/t186/public_sources.tbz2

tar xvf public_sources.tbz2
rm public_sources.tbz2
mv Linux_for_Tegra/source/public/kernel_src.tbz2 .
rm -rf Linux_for_Tegra/ 	 
tar xvf kernel_src.tbz2
rm kernel_src.tbz2
cd kernel/kernel-4.9/
zcat /proc/config.gz > .config
```
### Verify the Kernel Configuration
```
head -10 .config

CONFIG_ARM64=y
CONFIG_64BIT=y
CONFIG_ARCH_PHYS_ADDR_T_64BIT=y
CONFIG_MMU=y
CONFIG_DEBUG_RODATA=y
CONFIG_ARM64_PAGE_SHIFT=12

```

### Compile the kernel
```
make prepare
make modules_prepare
time make -j8 Image
time make -j8 modules
```

### Keep  the default Linux kernel version. Then we can compare it later to our own kernel.
```
uname -a
```

### Backup the old kernel image file
```
sudo cp /boot/Image /boot/Image.original
```

### Install the new kernel
```
cd ~/jetson-bsp-sources/kernel/kernel-4.9
sudo make modules_install
sudo cp arch/arm64/boot/Image /boot/Image
sudo reboot
```

### Verify that the kernel don't tegra at each name
```
uname -a
```

### Backup the kernel config
```
cd ~/jetson-bsp-sources/kernel/kernel-4.9
cp .config kernel.config.original
```

### Config the kernel
```
make menuconfig
Networking support --> Networking options --> Network packet filtering framework --> IP set support (SET TO M) --> enter --> set everything to M except the 256
Networking support --> Networking options --> Network packet filtering framework --> Core Netfilter Configuration --> Under the Netfilter Xtables support set with M the "set target and match support"
Networking support --> Networking options --> Network packet filtering framework -->  IPv6: Netfilter Configuration --> set with M the "rpfilter"
save the file
```

### Rebuild and reinstall the kernel
```
make prepare
make modules_prepare
time make -j8 Image
time make -j8 modules
sudo make modules_install
sudo cp arch/arm64/boot/Image /boot/Image
sudo reboot
```