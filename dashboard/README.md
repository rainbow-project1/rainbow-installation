# Dashboard Setup

1. Dashboard Setup is the last step of the whole setup procedure.
2. Clone the repository.
3. Go to ***dashboard*** directory and give execution rights to all scripts.
4. Update the following lines of the ***rainbow-dashboard.sh*** script with the appropriate values. *serverIP* is the master's ip, the other values are the **cjdns** credentials from the **Rainbow Setup - Master** setup step.

```
serverIP="<SERVER_IP>"
port="<SERVER_PORT>"
password="<PASSWORD>"
publicKey="<PUBLICKEY>"
serverHostname="<HOSTNAME>"
serverIPv6="<IPv6>"
```

5. Execute ***rainbow-dashboard.sh***
6. Update the following lines of the ***.env*** file with the appropriate values. Server here is the local machine where the setup takes place.

```
SERVER_IP=<SERVER_IP>
SERVER_BASE_PATH=<VOLUME_DATA_PATH>
```

7. Execute the following

```
# You need to be logged in to the docker private repo, so if you don't
docker login registry.ubitech.eu/maestro/maestro/

docker-compose up -d
```
