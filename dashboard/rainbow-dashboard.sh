#!/usr/bin/env bash

serverIP="<SERVER_IP>"
port="<SERVER_PORT>"
password="<PASSWORD>"
publicKey="<PUBLICKEY>"
serverHostname="<HOSTNAME>"
serverIPv6="<IPv6>"

RED='\033[0;31m'
CYAN='\033[0;36m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m' # No Color

DISTRO=$(lsb_release -d | awk -F"\t" '{print $2}')

if [ -z "${DISTRO##*UBUNTU*}" ] || [ -z "${DISTRO##*Ubuntu*}" ];then
  sudo apt-get update -y
  sudo apt-get install -y python3 python3-pip net-tools
  pip3 install netifaces
elif [ -z "${DISTRO##*DEBIAN*}" ] || [ -z "${DISTRO##*Debian*}" ];then
  sudo apt-get update -y
  sudo apt-get install -y python3 python3-pip net-tools python3-netifaces
  pip3 install netifaces
else
  echo "Distro '$DISTRO' is not supported until now."
fi

echo -e "${ORANGE}Run CJDNS installation script${NC}"
sh ./init-02-cjdns.sh
sleep 2s

echo -e "${ORANGE}Configure CJDNS worker with the proper credentials${NC}"
sudo python init-03-cjdns-worker.py $serverIP $port $password $publicKey $serverIPv6
sleep 2s

echo -e "${ORANGE}Restart CJDNS${NC}"
sudo service cjdns stop
sleep 2s
sudo service cjdns start
sleep 2s

echo -e "${ORANGE}Configure /etc/hosts${NC}"
sudo python3 init-04-configure-host.py worker $serverHostname $serverIPv6

echo -e "${ORANGE}Run Docker installation scripts${NC}"
if [ -z "${DISTRO##*UBUNTU*}" ] || [ -z "${DISTRO##*Ubuntu*}" ];then
  sh ./init-05-docker-ubuntu.sh
elif [ -z "${DISTRO##*DEBIAN*}" ] || [ -z "${DISTRO##*Debian*}" ];then
  sh ./init-05-docker-debian.sh
else
  echo "Distro '$DISTRO' is not supported until now."
fi
sh ./init-06-docker-configure.sh

echo -e "${ORANGE}Run Docker compose installation scripts${NC}"
if [ -z "${DISTRO##*UBUNTU*}" ] || [ -z "${DISTRO##*Ubuntu*}" ] || [ -z "${DISTRO##*DEBIAN*}" ] || [ -z "${DISTRO##*Debian*}" ];then
  sh ./init-13-docker-compose.sh
else
  echo "Distro '$DISTRO' is not supported until now."
fi


