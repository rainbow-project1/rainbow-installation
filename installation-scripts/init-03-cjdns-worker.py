import sys
import re
import shutil

if len(sys.argv) != 6:
    print("Run the script with ServerIP, Port, Password, PublicKey as arguments")
    sys.exit(-1)

currentFilePath = "/etc/cjdns/cjdroute.conf"
newFilePath = "/etc/cjdns/cjdroute_new.conf"
backupFilePath = "/etc/cjdns/cjdroute_backup.conf"
serverIP = sys.argv[1]
port = sys.argv[2]
password = sys.argv[3]
publicKey = sys.argv[4]
serverIPv6 = sys.argv[5]

currFile = open(currentFilePath, 'r')
Lines = currFile.readlines()
currFile.close()
newFile = open(newFilePath, 'w+')
counter = 0
for line in Lines:

    if counter == 0:
        matches = re.search(r'\"interfaces\"', line.strip())
        if matches != None:
            counter += 1
        newFile.write(line)
    elif counter == 1:
        matches = re.search(r'\"UDPInterface\"', line.strip())
        if matches != None:
            counter += 1
        newFile.write(line)
    elif counter == 2:
        newFile.write(line)
        matches = re.search(r'\"connectTo\"', line.strip())
        if matches != None:
            counter += 1
            tabs = "\t\t    "
            newFile.write(tabs + "\"" + str(serverIP) + ":" + str(port) + "\": {\n")
            newFile.write(tabs + "\t\"login\": \"default-login\",\n")
            newFile.write(tabs + "\t\"password\": \"" + str(password) + "\",\n")
            newFile.write(tabs + "\t\"publicKey\": \"" + str(publicKey) + "\",\n")
            newFile.write(tabs + "\t\"peerName\": \"your-name-goes-here\"\n")
            newFile.write(tabs + "}")
    elif counter == 3:
        counter += 1
        if "//" in line:
            newFile.write("\n")
        else:
            newFile.write(",\n")
        newFile.write(line)
    else:
        newFile.write(line)

newFile.close()
shutil.copy2(currentFilePath, backupFilePath) # complete target filename given
shutil.move(newFilePath, currentFilePath) # complete target filename given
