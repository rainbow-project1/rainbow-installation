import re
import socket

after = "authorizedPasswords"
filePath = "/etc/cjdns/cjdroute.conf"

file1 = open(filePath, 'r')
Lines = file1.readlines()
file1.close()
portMatch = "your.external.ip.goes.here"
port = None
password = None
publicKey = None

for line in Lines:
    if after.__class__ != bool and line.__contains__(after):
        after = True
    elif after.__class__ == bool and after:
        if port == None:
            matches = re.search(r'your.external.ip.goes.here:([0-9]+)', line.strip())
            if matches != None:
                port = matches.groups()[0]
                continue
        if password == None:
            matches = re.search(r'password\": \"([0-z]+)', line.strip())
            if matches != None:
                password = matches.groups()[0]
                continue
        if publicKey == None:
            matches = re.search(r'publicKey\": \"([0-z]+.+)\"', line.strip())
            if matches != None:
                publicKey = matches.groups()[0]
                continue

        if port != None and password != None and publicKey != None:
            break

print("Port : " + str(port))
print("Password : " + str(password))
print("PublicKey : " + str(publicKey))
print("Hostname : " + str(socket.gethostname()))

