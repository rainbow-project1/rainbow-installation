#!/usr/bin/env bash

# ---------------------------------------------------------------------
# Copyright © 2023 RAINBOW Consortium
#
# All rights reserved
# ----------------------------------------------------------------------

docker_server="<DOCKER_SERVER>"
docker_username="<DOCKER_USERNAME>"
docker_password="<DOCKER_PASSWORD>"
docker_email="<DOCKER_EMAIL>"

RED='\033[0;31m'
CYAN='\033[0;36m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
NC='\033[0m' # No Color

DISTRO=$(lsb_release -d | awk -F"\t" '{print $2}')

if [ -z "${DISTRO##*UBUNTU*}" ] || [ -z "${DISTRO##*Ubuntu*}" ];then
  sudo apt-get update -y
  sudo apt-get install -y python3 python3-pip net-tools
  pip3 install netifaces
elif [ -z "${DISTRO##*DEBIAN*}" ] || [ -z "${DISTRO##*Debian*}" ];then
  sudo apt-get update -y
  sudo apt-get install -y python3 python3-pip net-tools python3-netifaces
  pip3 install netifaces
else
  echo "Distro '$DISTRO' is not supported until now."
fi

echo -e "${ORANGE}Run CJDNS installation script${NC}"
sh ./init-02-cjdns.sh
sleep 2s

echo -e "${ORANGE}\nConfigure /etc/hosts${NC}"
sudo python3 init-04-configure-host.py master

echo -e "${ORANGE}\nRun Docker installation scripts${NC}"
if [ -z "${DISTRO##*UBUNTU*}" ] || [ -z "${DISTRO##*Ubuntu*}" ];then
  sh ./init-05-docker-ubuntu.sh
elif [ -z "${DISTRO##*DEBIAN*}" ] || [ -z "${DISTRO##*Debian*}" ];then
  sh ./init-05-docker-debian.sh
else
  echo "Distro '$DISTRO' is not supported until now."
fi
sh ./init-06-docker-configure.sh

echo -e "${ORANGE}\nRun Kubernetes installation scripts${NC}"
sleep 2s
if [ -z "${DISTRO##*UBUNTU*}" ] || [ -z "${DISTRO##*Ubuntu*}" ] ;then
  sh ./init-07-k8s-ubuntu.sh
elif [ -z "${DISTRO##*DEBIAN*}" ] || [ -z "${DISTRO##*Debian*}" ];then
  sh ./init-07-k8s-debian.sh
else
  echo "Distro '$DISTRO' is not supported until now."
fi

sleep 60s
sh ./init-08-k8s-master.sh
sleep 2s
python3 init-09-k8s-master-configure.py
sleep 2s

echo -e "${ORANGE}\nInstall Rainbow Orchestrator\n${CYAN}"
sh ./init-10-install-rainbow-orchestrator.sh $docker_server $docker_username $docker_password $docker_email

echo -e "${ORANGE}\nFetch CJDNS credentials\n${CYAN}"
python3 init-11-cjdns-master-credentials.py
python3 init-03-cjdns-ipv6.py

echo -e "${ORANGE}\nKubernetes join command\n${CYAN}"
python3 init-12-k8s-join-master.py
