#!/usr/bin/env bash
RED='\033[0;31m'
ORANGE='\033[0;33m'
NC='\033[0m' # No Color

if [ "$#" -ne 4 ]; then
    echo "${RED}Illegal number of parameters"
    echo "Run the script as follow: "
    echo "${ORANGE}    $0   <docker_server> <docker_username> <docker_password> <docker_email>${NC}"
    exit 1
fi
server=$1
username=$2
password=$3
email=$4

basePath=$(pwd)
kubectl create namespace rainbow-system
kubectl create secret docker-registry -n=rainbow-system regcred --docker-server=$server --docker-username=$username \
--docker-password=$password --docker-email=$email

git clone https://gitlab.com/rainbow-project1/rainbow-orchestration.git
cd rainbow-orchestration
kubectl apply -f ./deployment
cd $basePath
