#!/usr/bin/env bash

ORANGE='\033[0;33m'
NC='\033[0m' # No Color

echo "${ORANGE}\nConfiguring K8s init config${NC}"
ipv6=$(python3 -c 'import importlib; cjdnsIPv6 = importlib.import_module("init-03-cjdns-ipv6"); print(cjdnsIPv6.cjdnsIPv6Search())')
cp k8s-init-config-ipv6.yaml k8s-init-config-ipv6_backup.yaml
sed -i "s/masterIPv6/${ipv6}/g" k8s-init-config-ipv6.yaml
hostname=$(hostname)
sed -i "s/master-hostname/${hostname}/g" k8s-init-config-ipv6.yaml

echo "${ORANGE}\nBootstrapping K8s${NC}"
sudo kubeadm init --config=k8s-init-config-ipv6.yaml
sleep 2s
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
sudo apt-get install -y bash-completion
echo "source <(kubectl completion bash)" >> $HOME/.bashrc

echo "${ORANGE}\nApplying calico configuration to K8s${NC}"
kubectl apply -f calico.yaml
sleep 5s
