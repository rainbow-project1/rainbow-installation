#!/usr/bin/env bash

ORANGE='\033[0;33m'
NC='\033[0m' # No Color
echo "${ORANGE}\nInstalling Docker in Ubuntu${NC}"

DISTRO=$(arch)

sudo apt-get remove docker docker-engine docker.io -y
sudo apt-get update -y
sudo apt install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo -e "${ORANGE}Run Docker installation scripts${NC}"
if [ -z "${DISTRO##*x86_64*}" ] || [ -z "${DISTRO##*amd*}" ];then
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
elif [ -z "${DISTRO##*aarch64*}" ] ;then
  sudo add-apt-repository "deb [arch=arm64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
else
  echo "Distro '$DISTRO' is not supported until now."
fi
sudo apt-get  update -y
sudo apt install -y docker-ce docker-ce-cli containerd.io
sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl start docker
sudo systemctl enable docker
