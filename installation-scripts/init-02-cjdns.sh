#!/usr/bin/env bash
ORANGE='\033[0;33m'
NC='\033[0m' # No Color

echo "${ORANGE}\nEnable IPv6 forwarding${NC}"
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.ipv6.conf.all.forwarding        = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF
sudo sysctl --system

basePath=$(pwd)
echo "${ORANGE}\nInstall CJDNS${NC}"
sudo apt-get update -y
sudo apt-get install -y build-essential nodejs make gcc git python python3 net-tools
sudo wget --header "DEPLOY-TOKEN: icz5U7Ux1sd6L3qZxXj5" https://gitlab.com/api/v4/projects/30741221/packages/generic/cjdns/20.6/cjdns-v20.6.tar.gz -O /opt/cjdns.tar.gz
sudo tar -xzf /opt/cjdns.tar.gz -C /opt/
sudo rm /opt/cjdns.tar.gz
sudo mv /opt/cjdns-cjdns-v20.6/ /opt/cjdns
cd /opt/cjdns
sudo NO_TEST=1 Seccomp_NO=1 ./do
sudo mkdir -p /etc/cjdns
sudo chmod 755 /etc/cjdns
./cjdroute --genconf > ~/cjdroute.conf
sudo mv ~/cjdroute.conf /etc/cjdns/cjdroute.conf
sudo /opt/cjdns/cjdroute < /etc/cjdns/cjdroute.conf
cd $basePath
sudo cp cjdns.txt /etc/init.d/cjdns
sudo chmod +x /etc/init.d/cjdns
sleep 5
sudo update-rc.d cjdns defaults
sleep 5
sudo service cjdns stop
sleep 5
sudo service cjdns start
sleep 5