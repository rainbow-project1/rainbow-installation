#!/usr/bin/env bash

ORANGE='\033[0;33m'
NC='\033[0m' # No Color

echo "${ORANGE}\nInstall Kubernetes${NC}"
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add
sudo apt-get update -y
sleep 2s
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
sudo apt-get update -y
sleep 2s
sudo apt-get install -y kubeadm=1.21.1-00 kubelet=1.21.1-00 kubectl=1.21.1-00
sleep 2s
sudo apt-mark hold kubeadm kubelet kubectl

echo "${ORANGE}\nDisable swap${NC}"
sudo swapoff --all
sudo cp /etc/fstab /etc/fstab_backup
sudo sed -i.bak '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
