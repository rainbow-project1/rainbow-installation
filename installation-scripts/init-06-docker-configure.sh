#!/usr/bin/env bash

sudo mkdir -p /etc/docker
sudo touch /etc/docker/daemon.json
sudo cp /etc/docker/daemon.json /etc/docker/daemon_backup.json

cat <<EOF | sudo tee /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

sudo mkdir -p /etc/systemd/system/docker.service.d

# Restart Docker
sudo systemctl daemon-reload
sudo systemctl restart docker