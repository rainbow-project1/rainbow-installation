import sys
import socket
import shutil
import importlib
cjdnsIPv6 = importlib.import_module("init-03-cjdns-ipv6")

def checkArgs():
    if len(sys.argv) < 2 or not (sys.argv[1].__eq__("worker") or sys.argv[1].__eq__("master")) or \
            (sys.argv[1].__eq__("worker") and len(sys.argv) < 4):
        print("Run the script as follows: \n"
              "python init-04-configure-host.py master \n"
              "python init-04-configure-host.py worker <masterHostname> <masterIPv6> \n")
        sys.exit(-1)
    elif sys.argv[1].__eq__("worker"):
        if(sys.argv[2].__contains__(":")):
            print("Run the script as follows for worker: \n"
                  "python init-04-configure-host.py worker <masterHostname> <masterIPv6> \n")
            sys.exit(-1)
        return "worker", sys.argv[2], sys.argv[3]
    elif sys.argv[1].__eq__("master"):
        return "master",

def getHostname():
    return socket.gethostname()

def getIPv6():
    return cjdnsIPv6.cjdnsIPv6Search()

def backupFile():
    currentFilePath = "/etc/hosts"
    backupFilePath = "/etc/hosts_backup"
    shutil.copy2(currentFilePath, backupFilePath)  # complete target filename given

def addToHosts(hostname, ipv6 ):
    currentFilePath = "/etc/hosts"
    currFile = open(currentFilePath, 'a')
    currFile.write(ipv6 + " " + hostname + "\n")
    currFile.close()

if __name__ == "__main__":
    inputArgs = checkArgs()
    hostname = getHostname()
    ipv6 = getIPv6()
    backupFile()
    addToHosts(hostname, ipv6)
    if inputArgs[0] == "worker":
        addToHosts(inputArgs[1], inputArgs[2])