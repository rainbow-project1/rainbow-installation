import socket
import subprocess
import re
import sys

def executeCmd(cmd):
    subprocess_return = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    cmdOut = subprocess_return.stdout.read()
    cmdOut = cmdOut.decode("utf-8")
    return cmdOut

def createToken():
    cmd = "kubeadm token create --ttl 0"
    cmdOut = executeCmd(cmd)
    if cmdOut == None:
        print('\033[91m Error creating K8s token! \033[0m')
        sys.exit(-1)

    cmdOut = cmdOut.replace("\n", "")
    return cmdOut

def getCertificate():
    cmd = "openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/ˆ.* //' "
    cmdOut = executeCmd(cmd)
    matches = re.search(r'.stdin.= ([0-z]+)', str(cmdOut))
    if matches == None:
        print('\033[91m Error creating K8s certificate! \033[0m')
        sys.exit(-1)
    return matches.groups()[0]

def createJoinCmd(token, certificate):
    hostname = socket.gethostname()

    joinCmd = "sudo kubeadm join MASTER_HOSTNAME:6443 --token FILL_TOKEN --discovery-token-ca-cert-hash sha256:FILL_HASH"
    joinCmd = joinCmd.replace("FILL_TOKEN", token)
    joinCmd = joinCmd.replace("FILL_HASH", certificate)
    joinCmd = joinCmd.replace("MASTER_HOSTNAME", hostname)
    return joinCmd


if __name__ == "__main__":
    token = createToken()
    certificate = getCertificate()
    joinCmd = createJoinCmd(token, certificate)
    print(joinCmd)
