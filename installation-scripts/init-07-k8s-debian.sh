#!/usr/bin/env bash

ORANGE='\033[0;33m'
NC='\033[0m' # No Color

echo "${ORANGE}\nInstall Kubernetes${NC}"
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" \
 | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubeadm=1.21.1-00 kubelet=1.21.1-00 kubectl=1.21.1-00
sudo apt-mark hold kubeadm kubelet kubectl

echo "${ORANGE}\nDisable swap${NC}"
sudo swapoff --all
sudo cp /etc/fstab /etc/fstab_backup
sudo sed -i.bak '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
