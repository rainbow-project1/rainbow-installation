import netifaces as ni

def cjdnsIPv6Search():
    cjdnsIPv6 = None
    counter = 0
    while True:
        try:
            ipv6Addrs = ni.ifaddresses('tun' + str(counter))[ni.AF_INET6]
            for ipv6 in ipv6Addrs:
                if ipv6['addr'].startswith("fc"):
                    cjdnsIPv6 = ipv6['addr']
                    break
            counter += 1
        except Exception:
            break
    return cjdnsIPv6

if __name__ == "__main__":
    cjdnsIPv6 = cjdnsIPv6Search()
    print("CJDNS IPv6 : " + str(cjdnsIPv6))