import json
import os
import socket
import time
import subprocess


def getDnsDeployment(filePath):
    os.system("kubectl get  deployment.apps/coredns -n kube-system -o json > " + str(filePath))


def applyDnsDeployment(filePath):
    time.sleep(10)
    os.system("kubectl apply -f " + str(filePath))
    time.sleep(2)
    os.system("sudo systemctl disable systemd-resolved")
    time.sleep(2)
    os.system("sudo systemctl stop systemd-resolved")
    time.sleep(2)
    os.system("kubectl -n kube-system scale  deployments coredns --replicas=0")
    time.sleep(10)
    os.system("kubectl -n kube-system scale  deployments coredns --replicas=1")


def loadJson(filePath):
    jsonFile = open(filePath, )
    data = json.load(jsonFile)
    jsonFile.close()
    return data


def saveJson(filePath, jsonData):
    with open(filePath, 'w', encoding='utf-8') as f:
        json.dump(jsonData, f, ensure_ascii=False, indent=4)


def changeReplica(jsonData, replicaCount):
    jsonData.get("spec")["replicas"] = replicaCount


def changeDnsPolicy(jsonData, dnsPolicy):
    jsonData.get("spec")["template"]["spec"]["dnsPolicy"] = dnsPolicy


def networkMode(jsonData, mode=True):
    jsonData.get("spec")["template"]["spec"]["hostNetwork"] = mode


def changeNodeSelector(jsonData, nodeSelect):
    jsonData.get("spec")["template"]["spec"]["nodeSelector"]["kubernetes.io/hostname"] = nodeSelect


def executeCmd(cmd):
    subprocess_return = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    cmdOut = subprocess_return.stdout.read()
    cmdOut = cmdOut.decode("utf-8")
    return cmdOut


def checkReadines():
    cmd = "kubectl get nodes | grep " + socket.gethostname()
    while (True):
        cmdOut = executeCmd(cmd)
        cmdOut = cmdOut.replace("\n", "")
        print(cmdOut)
        if not "NotReady" in cmdOut:
            break
        time.sleep(5)

    cmd = "kubectl get pods -n kube-system | grep -wv Running"
    while (True):
        cmdOut = executeCmd(cmd)
        print(cmdOut)
        if not (("calico" in cmdOut) or ("etcd" in cmdOut) or ("kube" in cmdOut)):
            break
        time.sleep(5)

def checkNameResolution():
    cmd ="ping -c 1 google.com"
    cmdOut = executeCmd(cmd)
    cmdOut = cmdOut.replace("\n", "")
    print(cmdOut)
    if ("failure" in cmdOut) or (not "ttl" in cmdOut):
        return False
    return True

def fixNameServer(nameserverIP):
    cmd = "sudo sed -i 's/nameserver 127.0.0.53/nameserver " + nameserverIP + "/g' /etc/resolv.conf"
    executeCmd(cmd)

    if not checkNameResolution():
        cmd = "sudo sed -i -e '$anameserver " + nameserverIP + "' /etc/resolv.conf"
        executeCmd(cmd)
        return checkNameResolution()
    else:
        return True

if __name__ == "__main__":
    checkReadines()
    time.sleep(15)
    dnsFileName = "coreDnsDeployment.json"
    getDnsDeployment(dnsFileName)
    dnsJson = loadJson(dnsFileName)
    changeReplica(dnsJson, 1)
    changeDnsPolicy(dnsJson, "ClusterFirstWithHostNet")
    networkMode(dnsJson, True)
    hostname = socket.gethostname()
    changeNodeSelector(dnsJson, hostname)
    saveJson(dnsFileName, dnsJson)
    applyDnsDeployment(dnsFileName)
    if not checkNameResolution():
        fixNameServer("1.1.1.1")
